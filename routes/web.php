<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SearchComicController@index');

Route::match(['post','get'],'/getListBookByURL','SearchComicController@getListBookByURL');
Route::match(['post','get'],'/leechUrl','SearchComicController@leechUrl');



