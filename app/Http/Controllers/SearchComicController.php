<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

class SearchComicController extends Controller
{
    //
    function index(Request $request)
    {

        return view('SearchComic.index');
    }

    function getListBookByURL(Request $request)
    {
        $data = $request->all();
        if (!empty($data['url'])) {
            $client = new \GuzzleHttp\Client();
            $res = $client->get($data['url']);
            $body = $res->getBody();
            $remainingBytes = $body->getContents();

            $dom = HtmlDomParser::str_get_html($remainingBytes);

            $xpath = ".chapter-list .row span";
            $arrData = array();
            foreach ($dom->find($xpath) as $key => $items) {
                foreach ($items->find('a') as $k => $item) {
                    if ($k == 0) {
                        if(strpos($item, 'https://123link.co')  == false){
                            $arrData[] = $item->href;
                        }
                    }
                }
            }
            return $arrData;
        }
    }

    function leechUrl(Request $request){
        $data = $request->all();
        if(!empty($data['url'])){
            $client = new \GuzzleHttp\Client();
            $res = $client->get($data['url'],['time_out'=>3.14,'connection_timeout'=>3]);
            $body = $res->getBody();
            $remainingBytes = $body->getContents();

            $dom = HtmlDomParser::str_get_html($remainingBytes);

            $xpath = ".vung-doc";
            $listImg = array();
            foreach ($dom->find($xpath) as $items){
               dump($items->innertext());
            }
            die();
            return $listImg;
        }
    }
}
