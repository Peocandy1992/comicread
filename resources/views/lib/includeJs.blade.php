<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>


<script>
    $(document).ready(function () {
        /*** Ajax Post block **/
        function ajaxPostBlock(url, data, callBack, block, async) {
            /*** async = async || 'true'; **/
            /*** setup csrf for laravel proj **/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data,
                async: async,
                beforeSend: function () {
                    window.totR++;
                    /***Block UI. **/
                    if (block) {
                        $(block).block({
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            },
                            message: 'Đang load!!!',
                        });

                    }
                },
                success: function (res) {
                    if (res.error) {
                        alert(res.error);
                        return;
                    }
                    if (typeof callBack === 'function') callBack(res);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    /*** console.log(textStatus); **/
                },
                complete: function () {
                    window.totR--
                    /***Unblock UI. **/
                    if (block) {
                        $(block).unblock();

                    }
                }
            });
        }

        $('.submitSearchUrl').on('click', function (e) {
            e.preventDefault();
            var url = $('.inputUrl').val();
            ajaxPostBlock('/getListBookByURL', {url: url}, function (res) {
                $('.box_content').empty();
                $.each(res, function (k, v) {
                    $('.box_content').append('<div class="LeechTarget"><a href="' + v + '">' + v + '</a></div>');

                });
                LeechTargetUrl();
            });
        });

        var LeechTargetUrl = function(){
            $('.LeechTarget a').on('click',function(e){
                e.preventDefault();
                var url = $(this).attr('href');
                ajaxPostBlock('/leechUrl',{url:url},function (res) {
                    $('.box_content').empty().append(res);
                })
            })
        };

        LeechTargetUrl();

    });
</script>
