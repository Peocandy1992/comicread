<head>
    @include('lib.includeCss')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div class="container">
    <title>Đây là trang index của View Comic free</title>

    <div class="col-md-12 box-body" style="text-align: center;margin-top: 10%;">
        <div style="text-align: center;line-height: 30px">Đây là trang index của view comic free</div>

        <div class="row">
            <input type="text" class="inputUrl form-control col-md-6"/>

            <button class="btn btn-primary submitSearchUrl col-md-3" style="margin-left: 10px;" >
                Submit
            </button>
        </div>

        <div class="box_content">

        </div>
    </div>
</div>

@include('lib.includeJs')
</body>

